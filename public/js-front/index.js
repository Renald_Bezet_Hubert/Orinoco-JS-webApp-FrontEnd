/**
 * display all Cameras
 * fetch url ==> API
 * Camera in main.js
 **/

// HTML part
const container = document.getElementById('container')
const alertAccess = document.getElementById('alertAccess')

// display cameras
const displayAllCameras = camera => {
  container.innerHTML += `
  <article class="produit col-sm" style ="width: 18rem">
      <img class="card-img-top" src=${camera.imageUrl} alt="photos produits" />
      <div class="card-body">
        <h3 class="card-title">${camera.name}</h3>
        <p class="card-text">${camera.description}</p>
        <p>${camera.price / 100}€</p>
        <a href="html/product.html?id=${camera.id}" class="btn btn-secondary">Découvrir</a>
      </div>
  </article>`
}

// Call API
fetch('http://localhost:3000/api/cameras')
  .then(response => response.json())
  .then(function (allCameras) {
    allCameras.forEach(model => {
      const camera = new Camera(model)
      displayAllCameras(camera)
    })
  })
// IF ERROR API
  .catch((error) => {
    // console.log('fetch Error')
    alertAccess.innerHTML += `<div class="alert alert-warning" role="alert">
    <p class="alert-heading">Nous sommes désolés, les produits sont indisponibles pour le moment</p>
    <hr>
    <p class="mb-0">à bientôt</p>
  </div>`
  })
