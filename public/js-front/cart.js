/*
* Cart page
* resume &/or modify order
* send form to confirm order
*let cart = JSON.parse(storage.getItem("cart"))
* storage = localStorage
 */

// Get cart with storage
const storage = localStorage
const cart = JSON.parse(storage.getItem('cart')) ? JSON.parse(storage.getItem('cart')) : []

// data HTML
const containerTable = document.getElementById('containerTable')
const alertForm = document.getElementById('alertForm')

// Initialize price of cart
let priceCart = 0

// cart array
const addInCart = []

// Sum Price order and log result
function totalPriceCart (camera) {
  priceCart += camera.quantity * camera.price / 100
  const totalPrice = document.getElementById('totalPrice').textContent = `TOTAL: ${priceCart}€`
  storage.setItem('totalPrice', JSON.stringify(totalPrice))
};

// Display in html the cart
cart.forEach((camera, i) => {
  containerTable.innerHTML +=
  `
  <tbody>
    <tr>
      <td scope="rowspan"></td>
      <td  scope="row" class="pt-4">
        <div class="card mb-3 border border-primary border-3 rounded">
          <div class="row g-0">
            <div class="col-md-4">
              <img class="img-fluid" src=${camera.imageUrl} alt="Card image cap">
            </div>
            <div class="col-md-8 mt-2">
              <div class="card-body d-flex flex-column justify-content-around">
                <h5 class="text-uppercase px-1">${camera.name}</h5>
                <p class="text-uppercase px-1">${camera.price / 100}€</p>
              </div>
            </div>
          </div>
        </div>
      </td>
      <td></td>
      <td scope="row"><p class="fs-1 px-1 text-center">${camera.quantity}</p></td>
      <td></td>
      <td></td>
      <td scope="row">
      <p class="fs-1 px-1 text-center"><a href="#" class="deleteCamera" data-id="${i}"><i class="fas fa-minus-circle"></i></a></p>
      </td>
    </tr>
  </tbody>
  `

  // Call Price Cart
  totalPriceCart(camera)

  // add in cart array
  for (let i = 0; i < camera.quantity; i++) {
    addInCart.push(camera.id)
  }
})

// cart, delete product
function deleteCamera (id) {
  const camera = cart[id]
  if (camera.quantity > 1) {
    camera.quantity--
  } else {
    cart.splice(id, 1)
  }
  storage.setItem('cart', JSON.stringify(cart))
  window.location.reload()
}

// Delete button
document.querySelectorAll('.deleteCamera').forEach(deletedButton => {
  deletedButton.addEventListener('click', () => deleteCamera(deletedButton.dataset.id))
})

const deleteCart = document.getElementById('deleteCart')
deleteCart.addEventListener('click', deletedCart)

function deletedCart () {
  if (cart == null) {
  } else {
    containerTable.remove()
    storage.clear()
    window.location.reload()
  }
}

/**
 * FORM PART
 */
function sendOrder () {
  const form = document.getElementById('form')
  if (form.reportValidity() === true && addInCart.length > 0) {
    const contact = {
      firstName: document.getElementById('firstname').value,
      lastName: document.getElementById('lastname').value,
      address: document.getElementById('address').value,
      city: document.getElementById('city').value,
      email: document.getElementById('email').value
    }
    const products = addInCart
    const orderForm = JSON.stringify({
      contact,
      products
    })

    // Call Fetch
    fetch('http://localhost:3000/api/cameras/order', {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      mode: 'cors',
      body: orderForm
    })
      .then(function (response) {
        return response.json()
      })
      .then(function (response) {
        storage.setItem('contact', JSON.stringify(response.contact))
        window.location.assign(`order.html?orderId=${response.orderId}`)
      })
      // if error
      .catch((err) => {
        console.log('Error')
      })
  } else {
    alertForm.innerHTML += `<div class="alert alert-warning" role="alert">
    <p class="alert-heading">Une erreur est  survenue lors de votre commande</p>
    <hr>
    <p class="mb-0">Vérifiez vos informations, s'il vous plait.</p>
  </div>`
  }
}

// Form listener
const sendForm = document.getElementById('sendForm')

sendForm.addEventListener('click', function (event) {
  event.preventDefault()
  sendOrder()
})
