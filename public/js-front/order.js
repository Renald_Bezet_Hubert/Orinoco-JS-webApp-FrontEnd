const paramsUrl = new URL(window.location).searchParams
// get id order
const orderId = paramsUrl.get('orderId')
const storage = localStorage
// Get contact data
console.log(storage.getItem('contact'))
const contact = JSON.parse(storage.getItem('contact'))

// Get TOTAL PRICE
const totalPrice = JSON.parse(storage.getItem('totalPrice'))

// HTML part
const validateOrder = () => {
  const order = document.getElementById('order')
  order.innerHTML += `
        <div class="alert alert-success">
            <h4 class="alert-heading">Bravo ${contact.firstName} ${contact.lastName}, votre commande est validée</h4>
            <p>Nous avons bien reçu votre commande N° ${orderId}</p>
            <p class="fw-bold">${totalPrice}</p>  
            <hr>
            <p class="mb-0">Vous recevrez un mail à l'adresse: ${contact.email}</p>
        </div>
    `
}
validateOrder()
