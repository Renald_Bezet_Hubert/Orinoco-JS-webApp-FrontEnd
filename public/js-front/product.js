// get url id
const params = (new URL(document.location)).searchParams
// stock id
const id = params.get('id')

// get HTML element
const containerCard = document.getElementById('containerCard')
const alertDisplay = document.getElementById('alertDisplay')
// build HTML display camera
const displayCamera = camera => {
  containerCard.innerHTML += `
  <div class="card mb-3" style="max-width: 100%;">
  <div class="row g-0">
    <div class="col-md-4">
    <img class="card-img-top"  src=${camera.imageUrl}  alt="pic camera">
    </div>
    <div class="col-md-8">
      <div class="card-body">
      <h5 class="card-title ">${camera.name}</h5>
          <p class="card-text">${camera.description}</p>
          <select class="options" id ="option">
              <option>Choix des lentilles</option>
          </select>
          <select id="quantity">           
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
          </select> 
          <p class="price"> Prix Unitaire: ${camera.price / 100}€</p>
            <a href ="./cart.html"><button class="btn btn-primary" type ="submit" id="cart" value="submit">Ajouter au panier</button></a>
      </div>
    </div>
  </div>
</div>
    `
  // Choice lenses
  for (const lenses of camera.lenses) {
    document.getElementById('option').innerHTML +=
    `<option value="1">${lenses}</option>`
  }
  // get cart and listener on click
  document.getElementById('cart').addEventListener('click', function () {
    addCameraToCart(camera)
  })
}
//                        ***********Cart part******************
// Post to localStorage
const addToLocalStorage = cart => {
  localStorage.setItem('cart', JSON.stringify(cart))
}

// Add to cart
const addCameraToCart = camera => {
  camera.quantity = parseInt(document.getElementById('quantity').value)
  const cart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : []

  // condition to add to cart
  let cameraByIndex = false
  for (let i = 0; i < cart.length; i++) {
    const product = cart[i]
    // if product ok
    if (product.id === camera.id) {
      cameraByIndex = i
    }
  }
  // condition
  if (cameraByIndex !== false) {
    cart[cameraByIndex].quantity = parseInt(cart[cameraByIndex].quantity) + camera.quantity
  } else {
    cart.push(camera)
  }
  addToLocalStorage(cart)
}

// Fetch calling
fetch(`http://localhost:3000/api/cameras/${id}`)
  .then(response => response.json())
  .then(function (product) {
    const camera = new Camera(product)
    displayCamera(camera)
  })
  // if error
  .catch((error) => {
    // console.log('fetch Error')
    alertDisplay.innerHTML += `<div class="alert alert-warning" role="alert">
    <p class="alert-heading">Nous sommes désolés, le produit est indisponible pour le moment</p>
    <hr>
    <p class="mb-0">à bientôt</p>
  </div>`
  })
